/*
 * tdap/IirCoefficients.hpp
 *
 * Part of TdAP
 * Time-domain Audio Processing
 * Copyright (C) 2015 Michel Fleur.
 * Source https://bitbucket.org/emmef/tdap
 * Email  tdap@emmef.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TDAP_IIRCOEFFICIENTS_HEADER_GUARD
#define TDAP_IIRCOEFFICIENTS_HEADER_GUARD

#include <type_traits>
#include <cstddef>
#include "Denormal.hpp"
#include "Value.hpp"

namespace tdap {

template <typename F, size_t ORDER, bool flushToZero = false>
inline static F iir_filter_fixed(
		const F * const c,  // (ORDER + 1) C-coefficients
		const F * const d,  // (ORDER + 1) D-coefficients
		F * const xHistory, // (ORDER) x value history
		F * const yHistory, // (ORDER) y value history
		const F input)      // input sample value
{
	static_assert(std::is_floating_point<F>::value, "Coefficient type should be floating-point");
	static_assert(ORDER > 0, "ORDER of filter must be positive");

	F Y = 0;
	F X = input; // input is xN0
	F yN0 = 0.0;
	size_t i, j;
	for (i = 0, j = 1; i < ORDER; i++, j++) {
		const F xN1 = xHistory[i];
		const F yN1 = yHistory[i];
		xHistory[i] = X;
		X = xN1;
		yHistory[i] = Y;
		Y = yN1;
		yN0 += c[j] * xN1 + d[j] * yN1;
	}
	yN0 += c[0] * input;

	if (flushToZero) {
		Denormal::flush(yN0);
	}
	yHistory[0] = yN0;

	return yN0;
}

template <typename F, bool flushToZero = false>
inline static F iir_filter(
		int order,
		const F * const c,  // (order + 1) C-coefficients
		const F * const d,  // (order + 1) D-coefficients
		F * const xHistory, // (order) x value history
		F * const yHistory, // (order) y value history
		const F input)      // input sample value
{
	static_assert(std::is_floating_point<F>::value, "Coefficient type should be floating-point");

	F Y = 0;
	F X = input; // input is xN0
	F yN0 = 0.0;
	int i, j;
	for (i = 0, j = 1; i < order; i++, j++) {
		const F xN1 = xHistory[i];
		const F yN1 = yHistory[i];
		xHistory[i] = X;
		X = xN1;
		yHistory[i] = Y;
		Y = yN1;
		yN0 += c[j] * xN1 + d[j] * yN1;
	}
	yN0 += c[0] * input;

	if (flushToZero) {
		Denormal::flush(yN0);
	}
	yHistory[0] = yN0;

	return yN0;
}

template <typename F>
class AbstractIirCoefficients
{
	static_assert(std::is_floating_point<F>::value, "Type T must be a floating-point type");
public:
	static constexpr size_t coefficientsForOrder(size_t order) { return order + 1; }
	static constexpr size_t totalCoefficientsForOrder(size_t order) { return 2 * coefficientsForOrder(order); }
	static constexpr size_t historyForOrder(size_t order) { return order; }
	static constexpr size_t totalHistoryForOrder(size_t order) { return 2 * historyForOrder(order); }

	virtual size_t order() const = 0;
	virtual size_t maxOrder() const = 0;
	virtual void setC(size_t idx, const F coefficient) = 0;
	virtual void setD(size_t idx, const F coefficient) = 0;
	virtual F getC(size_t idx) const = 0;
	virtual F getD(size_t idx) const = 0;
	virtual F filter(F * const xHistory, F * const yHistory, const F input) const = 0;

	size_t coefficientCount() const { return coefficientsForOrder(order()); }
	size_t totalCoefficientsCount() const { return totalCoefficientsForOrder(order()); }
	size_t historyCount() const { return historyForOrder(order()); }
	size_t totalHistoryCount() const { return totalHistoryForOrder(order()); }

	virtual ~AbstractIirCoefficients() = default;
};

template <typename F, size_t ORDER>
class FixedIirCoefficients : public AbstractIirCoefficients<F>
{
	static_assert(std::is_floating_point<F>::value, "Coefficient type should be floating-point");
	static constexpr size_t COEFFS = AbstractIirCoefficients<F>::coefficientsForOrder(ORDER);
	static constexpr size_t TOTAL_COEEFS = AbstractIirCoefficients<F>::totalCoefficientsForOrder(ORDER);
	static constexpr size_t C_OFFSET = 0;
	static constexpr size_t D_OFFSET = COEFFS;
	static constexpr size_t HISTORY = AbstractIirCoefficients<F>::historyForOrder(ORDER);
	static constexpr size_t TOTAL_HISTORY = AbstractIirCoefficients<F>::totalHistoryForOrder(ORDER);

	constexpr size_t getCOffset(size_t idx) const { return Value<size_t>::valid_below(idx, COEFFS) + C_OFFSET; }
	constexpr size_t getDOffset(size_t idx) const { return Value<size_t>::valid_below(idx, COEFFS) + D_OFFSET; }

	F &C(size_t idx) { return data[getCOffset(idx)]; }
	F &D(size_t idx) { return data[getDOffset(idx)]; }
	const F &C(size_t idx) const { return data[getCOffset(idx)]; }
	const F &D(size_t idx) const { return data[getDOffset(idx)]; }

	const F * const unsafeC() const { return data + C_OFFSET; }
	const F * const unsafeD() const { return data + D_OFFSET; }


public:
	virtual size_t order() const override { return ORDER; }
	virtual size_t maxOrder() const override { return ORDER; }

	virtual void setC(size_t idx, const F coefficient) override { C(idx) = coefficient; }
	virtual void setD(size_t idx, const F coefficient) override { D(idx) = coefficient; }
	virtual F getC(size_t idx) const override { return C(idx); }
	virtual F getD(size_t idx) const override { return D(idx); }

	template<typename T>
	void assign(const AbstractIirCoefficients<T> &source)
	{
		if (source.order() == ORDER) {
			for (size_t i = 0; i < COEFFS; i++) {
				C(i) = source.getC(i);
				D(i) = source.getD(i);
			}
			return;
		}
		throw std::invalid_argument("Value not below threshold");
	}

	virtual ~FixedIirCoefficients() = default;

	template<bool flushToZero>
	F do_filter(
			F * const xHistory, // (ORDER) x value history
			F * const yHistory, // (ORDER) y value history
			F input) const
	{
		return iir_filter_fixed<F, ORDER, flushToZero>(
				unsafeC(),
				unsafeD(),
				xHistory,
				yHistory,
				input);
	}

	virtual F filter(F * const xHistory, F * const yHistory, const F input) const
	{
		return do_filter<false>(xHistory, yHistory, input);
	}


private:
	F data[TOTAL_COEEFS];
};

template <typename F>
class IirCoefficients : public AbstractIirCoefficients<F>
{
	const size_t maxOrder_;
	size_t order_;
	F * data_;

	const size_t getCBaseOffset() const { return 0; }
	const size_t getDBaseOffset() const { return maxOrder_ + 1; }

	const size_t getCOffset(size_t i) const { return getCBaseOffset() + Value<size_t>::valid_below_or_same(i, order_); }
	const size_t getDOffset(size_t i) const { return getDBaseOffset() + Value<size_t>::valid_below_or_same(i, order_); }

	F &C(size_t i) { return data_[getCOffset(i)]; }
	F &D(size_t i) { return data_[getDOffset(i)]; }
	const F &C(size_t i) const { return data_[getCOffset(i)]; }
	const F &D(size_t i) const { return data_[getDOffset(i)]; }

	const F * const unsafeC() const { return data_ + getCBaseOffset(); }
	const F * const unsafeD() const { return data_ + getDBaseOffset(); }

public:
	IirCoefficients(size_t maxOrder) :
		maxOrder_(Value<size_t>::valid_between(maxOrder,1,64)),
		order_(maxOrder_),
		data_(new F[AbstractIirCoefficients<F>::totalCoefficientsForOrder(maxOrder_)]) { }

	IirCoefficients(size_t maxOrder, size_t order) :
		maxOrder_(Value<size_t>::valid_between(maxOrder,1,64)),
		order_(Value<size_t>::valid_between(order, 1, maxOrder_)),
		data_(new F[AbstractIirCoefficients<F>::totalCoefficientsForOrder(maxOrder_)]) { }

	void setOrder(size_t order) { order_ = Value<size_t>::valid_between(order, 1, maxOrder_); }
	virtual size_t order() const override { return order_; }
	virtual size_t maxOrder() const override { return maxOrder_; }

	virtual void setC(size_t idx, const F coefficient) override { C(idx) = coefficient; }
	virtual void setD(size_t idx, const F coefficient) override { D(idx) = coefficient; }
	virtual F getC(size_t idx) const override { return C(idx); }
	virtual F getD(size_t idx) const override { return D(idx); }

	template<typename T>
	void assign(const AbstractIirCoefficients<T> &source)
	{
		setOrder(source.order());
		for (size_t i = 0; i <= order_; i++) {
			C(i) = source.getC(i);
			D(i) = source.getD(i);
		}
	}

	template<bool flushToZero = false>
	F do_filter(
			F * const xHistory, // (ORDER) x value history
			F * const yHistory, // (ORDER) y value history
			F input) const
	{
		return iir_filter<F, flushToZero>(
				order_,
				unsafeC(),
				unsafeD(),
				xHistory,
				yHistory,
				input);
	}

	virtual F filter(F * const xHistory, F * const yHistory, const F input) const
	{
		return do_filter<false>(xHistory, yHistory, input);
	}

	virtual ~IirCoefficients()
	{
		delete[] data_;
	}
};



} /* End of name space tdap */

#endif /* TDAP_IIRCOEFFICIENTS_HEADER_GUARD */
