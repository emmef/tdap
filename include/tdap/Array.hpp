/*
 * tdap/Array.hpp
 *
 * Part of TdAP
 * Time-domain Audio Processing
 * Copyright (C) 2015 Michel Fleur.
 * Source https://bitbucket.org/emmef/tdap
 * Email  tdap@emmef.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TDAP_ARRAY_HEADER_GUARD
#define TDAP_ARRAY_HEADER_GUARD

#include <tdap/ArrayTraits.hpp>
#include "debug.hpp"

namespace tdap {

TDAP_DEBUG_DEF_COUNT(ArrayInitMaxSizeAndSize)
TDAP_DEBUG_DEF_COUNT(ArrayInitMaxSize)
TDAP_DEBUG_DEF_COUNT(ArrayInitCopy)
TDAP_DEBUG_DEF_COUNT(ArrayInitMove)
TDAP_DEBUG_DEF_COUNT(ArrayInitRefMove)
TDAP_DEBUG_DEF_COUNT(ArrayDestructor)
TDAP_DEBUG_DEF_COUNT(ArrayRef)
TDAP_DEBUG_DEF_COUNT(ArrayRead)
TDAP_DEBUG_DEF_COUNT(ArrayUnsafeRef)
TDAP_DEBUG_DEF_COUNT(ArrayUnsafeRead)
TDAP_DEBUG_DEF_COUNT(ArrayPlus)
TDAP_DEBUG_DEF_COUNT(ArrayGetSize)

template <typename T>
class Array;

template <typename T>
class RefArray;

template <typename T, bool ownsData>
class BaseArray : public ArrayTraits<T, BaseArray<T,ownsData>>
{
	static_assert(TriviallyCopyable<T>::value, "Type must be trivial to copy, move or destroy and have standard layout");

	friend class ArrayTraits<T, BaseArray<T, ownsData>>;
	friend class RefArray<T>;

	size_t maxSize_;
	size_t size_ = 0;
	T * data_;

	static size_t validMaxSize(size_t size)
	{
		if (size > 0 && size <= Count<T>::max()) {
			return size;
		}
		if (size == 0) {
			throw std::invalid_argument("BaseArray: size must be positive");
		}
		throw std::invalid_argument("BaseArray: size too big");
	}
	static T * notNull(T * const ptr)
	{
		if (ptr) {
			return ptr;
		}
		throw std::invalid_argument("BaseArray: null pointer");
	}

	size_t validSize(size_t size)
	{
		if (size > 0 && size <= maxSize_) {
			return size;
		}
		if (size == 0) {
			throw std::invalid_argument("FixedCapArray: size must be positive");
		}
		throw std::invalid_argument("FixedCapArray: size too big");
	}

	size_t _traitGetSize() const { debugArrayGetSizeCall(); return size_; }
	size_t _traitGetCapacity() const { return maxSize_; }

	T& _traitRefAt(size_t i)
	{
		debugArrayRefCall();
		return data_[i];
	}

	const T& _traitRefAt(size_t i) const
	{
		debugArrayReadCall();
		return data_[i];
	}

	T * _traitUnsafeData()
	{
		debugArrayUnsafeRefCall();
		return &data_[0];
	}

	const T * _traitUnsafeData() const
	{
		debugArrayUnsafeReadCall();
		return &data_[0];
	}

	T * _traitPlus(size_t i) const
	{
		debugArrayPlusCall();
		return data_ + i;
	}

	void literalAssign(const BaseArray &source)
	{
		data_ = source.data_;
		maxSize_ = source.maxSize_;
		size_ = source.size_;
	}

	static constexpr bool _traitHasTrivialAddressing() { return true; }

protected:
	BaseArray(size_t maxSize, size_t size, T * const data) :
		maxSize_(validMaxSize(maxSize)),
		size_(validSize(size)),
		data_(ownsData ? new T(maxSize_) : notNull(data))
	{
		debugArrayInitMaxSizeAndSizeCall();
	}
	BaseArray(size_t maxSize, T * const data) : BaseArray(maxSize, maxSize, data)
	{
		debugArrayInitMaxSizeCall();
	}
	BaseArray(const BaseArray &source) : BaseArray(source.size())
	{
		debugArrayInitCopyCall();
		copy(source);
	}
	BaseArray(BaseArray &&moved) : maxSize_(moved.maxSize_), size_(moved.size_), data_(moved.data_)
	{
		debugArrayInitMoveCall();
		if (ownsData) {
			moved.data_ = nullptr;
		}
	}
	BaseArray(bool refMove, const BaseArray &moved) : maxSize_(moved.maxSize_), size_(moved.size_), data_(moved.data_)
	{
		debugArrayInitRefMoveCall();
	}

	using ArrayTraits<T, BaseArray<T,ownsData>>::copy;
	using ArrayTraits<T, BaseArray<T,ownsData>>::move;

public:

	template<typename ...V>
	void operator = (const ArrayTraits<T, V...> &source)
	{
		if (source.size() > ArrayTraits<T, BaseArray<T,ownsData>>::capacity()) {
			throw std::invalid_argument("BaseArray::assignment from larger array");
		}
		setSize(source.size());
		ArrayTraits<T, BaseArray<T,ownsData>>::copy(source);
	}

	void operator = (const BaseArray &source)
	{
		if (source.size() > ArrayTraits<T, BaseArray<T,ownsData>>::capacity()) {
			throw std::invalid_argument("BaseArray::assignment from larger array");
		}
		setSize(source.size());
		ArrayTraits<T, BaseArray<T,ownsData>>::copy(source);
	}

	size_t maxSize() const { return maxSize_ ; }

	void setSize(size_t newSize)
	{
		size_ = validSize(newSize);
	}

	~BaseArray()
	{
		debugArrayDestructorCall();
		if (ownsData && data_ != nullptr) {
			delete [] data_;
		}
	}
};

template <typename T>
class Array : public BaseArray<T, true>
{
	static_assert(TriviallyCopyable<T>::value, "Type must be trivial to copy, move or destroy and have standard layout");
	friend class ArrayTraits<T, Array<T>>;
	using Base = BaseArray<T, true>;

	Array * operator &(); // private and undefined.

public:
	using Base::capacity;
	using Base::copy;
	using Base::move;
	using Base::setSize;

	Array(size_t maxSize, size_t size) : BaseArray<T, true>(maxSize, size, new T(maxSize)) { }
	Array(size_t maxSize) : Array(maxSize, maxSize) { }
	Array(const Array &source) : Array(source.size()) { }
	Array(Array &&moved) : BaseArray<T, true>(moved) { }

	template<typename ...V>
	void operator = (const ArrayTraits<T, V...> &source)
	{
		Base::operator =(source);
	}

	void operator = (const Array &source)
	{
		Base::operator =(source);
	}

	void operator = (const RefArray<T> &source)
	{
		Base::operator =(source);
	}
};

template <typename T>
class RefArray : public BaseArray<T, false>
{
	static_assert(TriviallyCopyable<T>::value, "Type must be trivial to copy, move or destroy and have standard layout");
	friend class ArrayTraits<T, RefArray<T>>;
	using Base = BaseArray<T, false>;

	RefArray * operator &(); // private and undefined.

public:
	using Base::capacity;
	using Base::copy;
	using Base::move;
	using Base::setSize;

	RefArray(T * const data, size_t maxSize, size_t size) : BaseArray<T, false>(maxSize, size, data) { }
	RefArray(T * const data, size_t maxSize) : RefArray(data, maxSize, maxSize) { }
	RefArray(const RefArray &source) : BaseArray<T, false>(true, source) { }
	RefArray(RefArray &&moved) : BaseArray<T, true>(moved) { }

	template<typename ...V>
	void operator = (const ArrayTraits<T, V...> &source)
	{
		Base::operator =(source);
	}

	void operator = (const Array<T> &source)
	{
		Base::operator =(source);
	}

	void operator = (const RefArray &source)
	{
		Base::literalAssign(source);
	}
};


} /* End of name space tdap */

#endif /* TDAP_ARRAY_HEADER_GUARD */
