/*
 * test_count.cpp
 *
 * Part of TdAP
 * Time-domain Audio Processing
 * Copyright (C) 2015 Michel Fleur.
 * Source https://bitbucket.org/emmef/tdap
 * Email  tdap@emmef.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>
#include <cstdio>
#include "../include/tdap/Count.hpp"
#include "TestCount.hpp"

using namespace tdap;
using namespace std;

template <size_t SIZE>
static void sizedCountSizedTestMax()
{
	static char displaySize[100];
	snprintf(displaySize, 100, "%zu", SIZE);

	size_t absoluteMax = std::numeric_limits<size_t>::max();
	size_t expectedMax = absoluteMax / SIZE;
	size_t actualMax = CountOfSize<SIZE>::max();
	string message = "Testing max() for elements of size ";
	message += displaySize;

	CPPUNIT_ASSERT_EQUAL_MESSAGE(message, expectedMax, actualMax);
}

void TestCount::countSizedTestMax()
{
	sizedCountSizedTestMax<1>();
	sizedCountSizedTestMax<2>();
	sizedCountSizedTestMax<3>();
	sizedCountSizedTestMax<5>();
	sizedCountSizedTestMax<7>();
	sizedCountSizedTestMax<11>();
}

template <size_t SIZE>
static void sizedCountSizedTestValid()
{
	static char displaySize[100];
	snprintf(displaySize, 100, "%zu", SIZE);
	string message = "Testing CountOfSize<";
	message += displaySize;
	message += ">::valid(N)";

	size_t absoluteMax = std::numeric_limits<size_t>::max();
	size_t max = CountOfSize<SIZE>::max();

	CPPUNIT_ASSERT_MESSAGE(message + " for N = 0", CountOfSize<SIZE>::valid(0));
	CPPUNIT_ASSERT_MESSAGE(message + " for values N = max()", CountOfSize<SIZE>::valid(max));
	double n;

	for (n = 1.0; n < max; n *= 1.5) {
		size_t N = n;
		CPPUNIT_ASSERT_MESSAGE(message + " for values N < max()", CountOfSize<SIZE>::valid(N));
	}

	CPPUNIT_ASSERT_MESSAGE(message + " for values N = max()", CountOfSize<SIZE>::valid(max));

	for (; n < absoluteMax; n *= 1.5) {
		size_t N = n;
		CPPUNIT_ASSERT_MESSAGE(message + " for values N < max()", !CountOfSize<SIZE>::valid(N));
	}
}

void TestCount::countSizedTestValid()
{
	sizedCountSizedTestValid<1>();
	sizedCountSizedTestValid<2>();
	sizedCountSizedTestValid<3>();
	sizedCountSizedTestValid<5>();
	sizedCountSizedTestValid<7>();
	sizedCountSizedTestValid<11>();
}

template <size_t SIZE>
static void sizedCountSizedTestValidPositive()
{
	static char displaySize[100];
	snprintf(displaySize, 100, "%zu", SIZE);
	string message = "Testing CountOfSize<";
	message += displaySize;
	message += ">::valid_positive(N)";

	size_t absoluteMax = std::numeric_limits<size_t>::max();
	size_t max = CountOfSize<SIZE>::max();

	CPPUNIT_ASSERT_MESSAGE(message + " for N = 0", !CountOfSize<SIZE>::valid_positive(0));
	CPPUNIT_ASSERT_MESSAGE(message + " for values N = max()", CountOfSize<SIZE>::valid_positive(max));
	double n;

	for (n = 1.0; n < max; n *= 1.5) {
		size_t N = n;
		CPPUNIT_ASSERT_MESSAGE(message + " for values N < max()", CountOfSize<SIZE>::valid_positive(N));
	}

	CPPUNIT_ASSERT_MESSAGE(message + " for values N = max()", CountOfSize<SIZE>::valid_positive(max));

	for (; n < absoluteMax; n *= 1.5) {
		size_t N = n;
		CPPUNIT_ASSERT_MESSAGE(message + " for values N < max()", !CountOfSize<SIZE>::valid_positive(N));
	}
}

void TestCount::countSizedTestValidPositive()
{
	sizedCountSizedTestValidPositive<1>();
	sizedCountSizedTestValidPositive<2>();
	sizedCountSizedTestValidPositive<3>();
	sizedCountSizedTestValidPositive<5>();
	sizedCountSizedTestValidPositive<7>();
	sizedCountSizedTestValidPositive<11>();
}

template <size_t SIZE>
static void sizedCountSizedTestProduct2Reverse(string message, size_t cnt1, size_t cnt2, size_t expected)
{
	static char displaySize[30];
	std::string specific = message;

	snprintf(displaySize, 30, "%zu", cnt1);
	specific += "; for N=";
	specific += displaySize;
	snprintf(displaySize, 30, "%zu", cnt2);
	specific += " and M=";
	specific += displaySize;
	size_t result1 = CountOfSize<SIZE>::product(cnt1, cnt2);
	size_t result2 = CountOfSize<SIZE>::product(cnt2, cnt1);

	CPPUNIT_ASSERT_EQUAL_MESSAGE(specific + "; expected product(N,M) = product(M,N)", result1, result2);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(specific, expected, result1);
}

template <size_t SIZE>
static void sizedCountSizedTestProduct2()
{
	static char displaySize[100];
	size_t max = CountOfSize<SIZE>::max();

	snprintf(displaySize, 100, "%zu", SIZE);
	string message = "CountOfSize<";
	message += displaySize;
	message += "> ";
	snprintf(displaySize, 100, "%zu", max);
	message += "::max()=";
	message += displaySize;
	message += " testing ::product(N,M)";
	sizedCountSizedTestProduct2Reverse<SIZE>(message + " for M=N=0", 0, 0, 0);

	double n, m;

	string specific1;

	specific1 = message;
	specific1 += " for M = 0, N != 0";
	for (m = 0, n = 1.0; n < max; n *= 1.5) {
		sizedCountSizedTestProduct2Reverse<SIZE>(specific1, m, n, 0);
	}
	specific1 = message;
	specific1 += " for M = 1, N = max";
	sizedCountSizedTestProduct2Reverse<SIZE>(specific1, 1, max, max);

	string specific2;

	specific1 = message;
	specific1 += " for M * N <= max";

	specific2 = message;
	specific2 += " for M * N > max";

	for (n = 1.0; n < max; n *= 1.7) {
		size_t N = n;
		for (m = 1.0; m < max; m *= 1.3) {
			size_t M = m;
			size_t maxN = max / M;

			if (N <= maxN) {
				sizedCountSizedTestProduct2Reverse<SIZE>(specific1, N, M, N * M);
			}
			else {
				sizedCountSizedTestProduct2Reverse<SIZE>(specific2, N, M, 0);
			}
		}
	}
}

void TestCount::countSizedTestProduct2()
{
	sizedCountSizedTestProduct2<1>();
	sizedCountSizedTestProduct2<2>();
	sizedCountSizedTestProduct2<3>();
	sizedCountSizedTestProduct2<5>();
	sizedCountSizedTestProduct2<7>();
	sizedCountSizedTestProduct2<11>();
	sizedCountSizedTestProduct2<13>();
	sizedCountSizedTestProduct2<17>();
}


void TestCount::test_check_index_happy()
{
	constexpr size_t SIZE = 1000;
	std::string message = "check_index(i, N) with i < N must not throw";
	for (size_t i = 0; i < SIZE; i++) {
		CPPUNIT_ASSERT_NO_THROW_MESSAGE(message, IndexPolicy::force(i, SIZE));
	}
}

void TestCount::test_check_index_is_size()
{
	constexpr size_t SIZE = 1000;
	std::string message = "check_index(i, N) with i == N must throw";
	CPPUNIT_ASSERT_THROW_MESSAGE(message, IndexPolicy::force(SIZE, SIZE), std::out_of_range);
}

void TestCount::test_check_index_larger_than__size()
{
	constexpr size_t SIZE = 1000;
	std::string message = "check_index(i, N) with i > N must throw";
	CPPUNIT_ASSERT_THROW_MESSAGE(message, IndexPolicy::force(SIZE + 1, SIZE), std::out_of_range);
}



void TestCount::setUp()
{
	// Nothing to implement
}

void TestCount::tearDown()
{
	// Nothing to implement
}

TestCount::~TestCount()
{
	// Nothing to implement
}
