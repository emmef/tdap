/*
 * TestIirCoefficients.hpp
 *
 * Defines unit tests for tdap/IirCoefficients.hpp.
 *
 * Part of TdAP
 * Time-domain Audio Processing
 * Copyright (C) 2015 Michel Fleur.
 * Source https://bitbucket.org/emmef/tdap
 * Email  tdap@emmef.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TEST__TDAP_IIRCOEFFICIENTS_HEADER_GUARD
#define TEST__TDAP_IIRCOEFFICIENTS_HEADER_GUARD

#include <cppunit/TestCaller.h>
#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>

/*
 * Do NOT include <tdap/IirCoefficients.hpp> here as it will
 * slow down the build process considerably.
 * The file
 *   IirCoefficients.cpp
 * that implements this test fixture will include the header.
 */
using namespace CppUnit;

class TestIirCoefficients : public TestFixture
{
public:
	// Assignment
	void testAssignmentSame();
	void testAssignmentFloatFixedToNonFixed();
	void testAssignmentFloatNonFixedToFixed();
	void testAssignmentDoubleToFloat();
	void testAssignmentFloatToDouble();
	void testAssignmentFixedToNonFixedOther();
	void testAssignmentNonFixedToFixedOther();

	// Order
	void testOrderForFixed();
	void testMaxOrderForFixed();
	void testAssignmentSmallerOrderForFixed();
	void testAssignmentBiggerOrderForFixed();

	void testSetOrder();
	void testSetOrderZero();
	void testSetOrderTooBig();
	void testAssignBiggerOrder();
	void testAssignSmallerOrder();
	void testAssignTooBigOrder();

	static TestSuite *createSuite()
	{
		CppUnit::TestSuite *suite = new CppUnit::TestSuite("Test suite for 'tdap/IirCoefficients.hpp'");


		suite->addTest(
				new TestCaller<TestIirCoefficients>("TestIirCoefficients test 'testAssignmentSame'",
						&TestIirCoefficients::testAssignmentSame));
		suite->addTest(
				new TestCaller<TestIirCoefficients>("TestIirCoefficients test 'testAssignmentFloatFixedNonFixed'",
						&TestIirCoefficients::testAssignmentFloatFixedToNonFixed));
		suite->addTest(
				new TestCaller<TestIirCoefficients>("TestIirCoefficients test 'testAssignmentFloatNonFixedFixed'",
						&TestIirCoefficients::testAssignmentFloatNonFixedToFixed));
		suite->addTest(
				new TestCaller<TestIirCoefficients>("TestIirCoefficients test 'testAssignmentDoubleFloat'",
						&TestIirCoefficients::testAssignmentDoubleToFloat));
		suite->addTest(
				new TestCaller<TestIirCoefficients>("TestIirCoefficients test 'testAssignmentFloatDouble'",
						&TestIirCoefficients::testAssignmentFloatToDouble));
		suite->addTest(
				new TestCaller<TestIirCoefficients>("TestIirCoefficients test 'testAssignmentFloatFixedDoubleNonFixed'",
						&TestIirCoefficients::testAssignmentFixedToNonFixedOther));
		suite->addTest(
				new TestCaller<TestIirCoefficients>("TestIirCoefficients test 'testAssignmentFloatNonFixedDoubleFixed'",
						&TestIirCoefficients::testAssignmentNonFixedToFixedOther));

		// Order

		suite->addTest(
				new TestCaller<TestIirCoefficients>("TestIirCoefficients test 'testOrderForFixed'",
						&TestIirCoefficients::testOrderForFixed));
		suite->addTest(
				new TestCaller<TestIirCoefficients>("TestIirCoefficients test 'testMaxOrderForFixed'",
						&TestIirCoefficients::testMaxOrderForFixed));
		suite->addTest(
				new TestCaller<TestIirCoefficients>("TestIirCoefficients test 'testAssignmentSmallerOrderForFixed'",
						&TestIirCoefficients::testAssignmentSmallerOrderForFixed));
		suite->addTest(
				new TestCaller<TestIirCoefficients>("TestIirCoefficients test 'testAssignmentBiggerOrderForFixed'",
						&TestIirCoefficients::testAssignmentBiggerOrderForFixed));

		suite->addTest(
				new TestCaller<TestIirCoefficients>("TestIirCoefficients test 'testSetOrder'",
						&TestIirCoefficients::testSetOrder));
		suite->addTest(
				new TestCaller<TestIirCoefficients>("TestIirCoefficients test 'testSetOrderZero'",
						&TestIirCoefficients::testSetOrderZero));
		suite->addTest(
				new TestCaller<TestIirCoefficients>("TestIirCoefficients test 'testSetOrderTooBig'",
						&TestIirCoefficients::testSetOrderTooBig));

		suite->addTest(
				new TestCaller<TestIirCoefficients>("TestIirCoefficients test 'testAssignBiggerOrder'",
						&TestIirCoefficients::testAssignBiggerOrder));
		suite->addTest(
				new TestCaller<TestIirCoefficients>("TestIirCoefficients test 'testAssignSmallerOrder'",
						&TestIirCoefficients::testAssignSmallerOrder));
		suite->addTest(
				new TestCaller<TestIirCoefficients>("TestIirCoefficients test 'testAssignTooBigOrder'",
						&TestIirCoefficients::testAssignTooBigOrder));

		return suite;
	}

	void setUp();
	void tearDown();
	virtual ~TestIirCoefficients();
};

#endif /* TEST__TDAP_IIRCOEFFICIENTS_HEADER_GUARD */
