/*
 * TestArray.cpp
 *
 * Part of TdAP
 * Time-domain Audio Processing
 * Copyright (C) 2015 Michel Fleur.
 * Source https://bitbucket.org/emmef/tdap
 * Email  tdap@emmef.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <random>
//#define TDAP_DEBUG_FACILITY_VERBOSE 1
#undef TDAP_DEBUG_FACILITY_VERBOSE
#include <tdap/Array.hpp>
#include "TestArray.hpp"

using namespace tdap;

void TestArray::testWithMaxSize()
{
	size_t capacity = Value<size_t>::min(Count<double>::max(), 15);
	Array<double> instance(capacity);

	CPPUNIT_ASSERT_EQUAL_MESSAGE("Array capacity equal to capacity parameter", capacity, instance.capacity());
	CPPUNIT_ASSERT_EQUAL_MESSAGE("Array initial size equal to capacity", instance.capacity(), instance.size());
}

void TestArray::testWithMaxSizeZero()
{
	CPPUNIT_ASSERT_THROW_MESSAGE("Array of capacity 0 not allowed", Array<int>(0), std::invalid_argument);
}

void TestArray::testWithMaxSizeTooLarge()
{
	CPPUNIT_ASSERT_THROW_MESSAGE("Array of capacity > Count<T>::max() not allowed", Array<double>(Count<double>::max() + 1), std::invalid_argument);;
}

void TestArray::testWithSameSize()
{
	size_t capacity = Value<size_t>::min(Count<double>::max(), 15);
	Array<double> instance(capacity, capacity);

	CPPUNIT_ASSERT_EQUAL_MESSAGE("Array capacity equal to capacity parameter", capacity, instance.capacity());
	CPPUNIT_ASSERT_EQUAL_MESSAGE("Array size equal to capacity, as constructed", instance.capacity(), instance.size());
}

void TestArray::testWithOtherSize()
{
	size_t capacity = Value<size_t>::min(Count<double>::max(), 15);
	size_t size = capacity - 3;
	Array<double> instance(capacity, size);

	CPPUNIT_ASSERT_EQUAL_MESSAGE("Array capacity equal to capacity parameter", capacity, instance.capacity());
	CPPUNIT_ASSERT_EQUAL_MESSAGE("Array size equal to size as constructed", size, instance.size());
}

void TestArray::testWithOtherSizeZero()
{
	size_t capacity = Value<size_t>::min(Count<double>::max(), 15);

	CPPUNIT_ASSERT_THROW_MESSAGE("Array of capacity 0 not allowed", Array<double>(capacity, 0), std::invalid_argument);
}

void TestArray::testWithOtherSizeTooLarge()
{
	size_t capacity = Value<size_t>::min(Count<double>::max(), 15);

	CPPUNIT_ASSERT_THROW_MESSAGE("Array of capacity and size > capacity not allowed", Array<double>(capacity, capacity + 1), std::invalid_argument);
}


void TestArray::testSetSize()
{
	size_t capacity = Value<size_t>::min(Count<double>::max(), 15);
	size_t size = capacity - 3;
	Array<double> instance(capacity);

	CPPUNIT_ASSERT_NO_THROW_MESSAGE("Array setting valid size does not throw", instance.setSize(size));
	CPPUNIT_ASSERT_EQUAL_MESSAGE("Array setSize(x) returned by size()", size, instance.size());
	CPPUNIT_ASSERT_EQUAL_MESSAGE("Array maxSize(x) not affected by setSize()", capacity, instance.capacity());
}

void TestArray::testSetSizeZero()
{
	size_t capacity = Value<size_t>::min(Count<double>::max(), 15);
	Array<double> instance(capacity);

	CPPUNIT_ASSERT_THROW_MESSAGE("Array setting size to zero throws", instance.setSize(0), std::invalid_argument);
}

void TestArray::testSetSizeTooLarge()
{
	size_t capacity = Value<size_t>::min(Count<double>::max(), 15);
	size_t size = capacity + 1;
	Array<double> instance(capacity);

	CPPUNIT_ASSERT_THROW_MESSAGE("Array setting size too large throws", instance.setSize(size), std::invalid_argument);
}

template<typename T>
void assertEquals(const Array<T> &a1, const Array<T> &a2)
{
	CPPUNIT_ASSERT_MESSAGE("Test flawed: same object instance passed", &a1 != &a2);
	CPPUNIT_ASSERT_EQUAL_MESSAGE("Arrays have same size", a1.size(), a2.size());

	for (size_t i = 0; i < a1.size(); i++) {
		CPPUNIT_ASSERT_EQUAL_MESSAGE("Arrays elements equal", 13, 13);
	}
}

void TestArray::testInitialAssignment()
{
	debugArrayResetCounts();
	size_t capacity = Value<size_t>::min(Count<int>::max(), 5);
	Array<int> instance(capacity);

	for (size_t i = 0; i < instance.size(); i++) {
		instance[i] = std::rand();
	}

	Array<int> copy(instance);

	assertEquals(instance, copy);
}

void TestArray::testInitialAssignmentDifferentSize()
{
	debugArrayResetCounts();
	size_t capacity = Value<size_t>::min(Count<int>::max(), 5);
	Array<int> instance(capacity);

	for (size_t i = 0; i < instance.size(); i++) {
		instance[i] = std::rand();
	}

	size_t newSize = 3;

	instance.setSize(newSize);

	Array<int> copy(instance);

	CPPUNIT_ASSERT_EQUAL_MESSAGE("Copy has current size, not maxSize", newSize, copy.size());

	assertEquals(instance, copy);

	instance = copy;
}


void TestArray::setUp()
{
	// Nothing to implement
}

void TestArray::tearDown()
{
	// Nothing to implement
}

TestArray::~TestArray()
{
	// Nothing to implement
}

