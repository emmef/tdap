/*
 * test_count.hpp
 *
 * Defines unit tests for tdap/Count.hpp.
 *
 * Part of TdAP
 * Time-domain Audio Processing
 * Copyright (C) 2015 Michel Fleur.
 * Source https://bitbucket.org/emmef/tdap
 * Email  tdap@emmef.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TEST__TDAP_COUNT_HEADER_GUARD
#define TEST__TDAP_COUNT_HEADER_GUARD

#include <cppunit/TestCaller.h>
#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>

/*
 * Do NOT include <tdap/count.hpp> here as it will
 * slow down the build process considerably.
 * The file
 *   test_count.cpp
 * that implements this test fixture will include the header.
 */
using namespace CppUnit;

class TestCount : public TestFixture
{
public:
	void countSizedTestMax();
	void countSizedTestValid();
	void countSizedTestValidPositive();
	void countSizedTestProduct2();

	void test_check_index_happy();
	void test_check_index_is_size();
	void test_check_index_larger_than__size();

	static TestSuite *createSuite()
	{
		CppUnit::TestSuite *suite = new CppUnit::TestSuite("Test suite for 'tdap/Count.hpp'");

		suite->addTest(
				new TestCaller<TestCount>("TestCount test 'countSizedTestMax'",
						&TestCount::countSizedTestMax));
		suite->addTest(
				new TestCaller<TestCount>("TestCount test 'countSizedTestValid'",
						&TestCount::countSizedTestValid));
		suite->addTest(
				new TestCaller<TestCount>("TestCount test 'countSizedTestValidPositive'",
						&TestCount::countSizedTestValidPositive));
		suite->addTest(
				new TestCaller<TestCount>("TestCount test 'countSizedTestProduct2'",
						&TestCount::countSizedTestProduct2));
		suite->addTest(
				new TestCaller<TestCount>("TestCount test 'test_check_index_happy'",
						&TestCount::test_check_index_happy));
		suite->addTest(
				new TestCaller<TestCount>("TestCount test 'test_check_index_is_size'",
						&TestCount::test_check_index_is_size));
		suite->addTest(
				new TestCaller<TestCount>("TestCount test 'test_check_index_larger_than__size'",
						&TestCount::test_check_index_larger_than__size));

		return suite;
	}

	void setUp();
	void tearDown();
	virtual ~TestCount();
};

#endif /* TEST__TDAP_COUNT_HEADER_GUARD */
