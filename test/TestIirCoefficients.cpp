/*
 * TestIirCoefficients.cpp
 *
 * Part of TdAP
 * Time-domain Audio Processing
 * Copyright (C) 2015 Michel Fleur.
 * Source https://bitbucket.org/emmef/tdap
 * Email  tdap@emmef.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <random>
#include "../include/tdap/IirCoefficients.hpp"
#include "TestIirCoefficients.hpp"
#include "../include/tdap/Value.hpp"

using namespace tdap;

void testIfSomeIirCoefficientsConstructsCompile()
{
	FixedIirCoefficients<float, 4> ff1;
	IirCoefficients<float> if1(4);
	FixedIirCoefficients<double, 4> fd1;
	IirCoefficients<double> id1(4);
	float floatX[5];
	float floatY[5];
	double doubleX[5];
	double doubleY[5];

	ff1.do_filter<false>(floatX, floatY, 1.0);
	if1.do_filter<false>(floatX, floatY, 1.0);
	fd1.do_filter<false>(doubleX, doubleY, 1.0);
	id1.do_filter<false>(doubleX, doubleY, 1.0);

	ff1.filter(floatX, floatY, 1.0);
	if1.filter(floatX, floatY, 1.0);
	fd1.filter(doubleX, doubleY, 1.0);
	id1.filter(doubleX, doubleY, 1.0);

	ff1.coefficientCount();
	if1.coefficientCount();
	fd1.coefficientCount();
	id1.coefficientCount();

	ff1.historyCount();
	if1.historyCount();
	fd1.historyCount();
	id1.historyCount();
}

void TestIirCoefficients::setUp()
{
	// Nothing to implement
}

void TestIirCoefficients::tearDown()
{
	// Nothing to implement
}

TestIirCoefficients::~TestIirCoefficients()
{
	// Nothing to implement
}


static double newRandom()
{
	return -0.5 + 0.5 * std::rand() / RAND_MAX;
}

template<typename T1, typename T2>
static constexpr double epsilonFor()
{
	// We assume 3/4 of bits minus one mantissa and one extra to be sure
	return Value<double>::max(0.5 / RAND_MAX,
			Value<double>::max(pow(0.5, 0.75 * 8 *sizeof(T1) - 2), pow(0.5, 0.75 * 8 * sizeof(T2) - 2)));
}

template<typename T>
void setRandomValues(AbstractIirCoefficients<T> &coefficients)
{
	for (size_t i = 0; i < coefficients.order(); i++) {
		coefficients.setC(i, newRandom());
		coefficients.setD(i, newRandom());
	}
}

template<typename T1, typename T2>
void assertEqual(const AbstractIirCoefficients<T1> &data1, const AbstractIirCoefficients<T2> &data2)
{
	CPPUNIT_ASSERT_MESSAGE("Coefficients not same object", static_cast<const void*>(&data1) != static_cast<const void*>(&data2));
	CPPUNIT_ASSERT_MESSAGE("Coefficients have same order", data1.order() == data2.order());

	double epsilon = epsilonFor<T1, T2>();
	for (size_t i = 0; i < data1.order(); i++) {
		if (!Value<double>::relative_distance_within(data1.getC(i), data2.getC(i), epsilon)) {
			std::string message = "Difference in C-coefficient ";
			message += ('0' + i);
			CPPUNIT_ASSERT_EQUAL_MESSAGE(message, (double)data1.getC(i), (double)data2.getC(i));
		}
		if (!Value<double>::relative_distance_within(data1.getD(i), data2.getD(i), epsilon)) {
			std::string message = "Difference in C-coefficient ";
			message += ('0' + i);
			CPPUNIT_ASSERT_EQUAL_MESSAGE(message, (double)data1.getD(i), (double)data2.getD(i));
		}
	}
}

template<typename T1, typename T2>
void assertNotEqual(const AbstractIirCoefficients<T1> &data1, const AbstractIirCoefficients<T2> &data2)
{
	CPPUNIT_ASSERT_MESSAGE("Coefficients not same object", static_cast<const void*>(&data1) != static_cast<const void*>(&data2));
	CPPUNIT_ASSERT_MESSAGE("Coefficients have same order", data1.order() == data2.order());

	double epsilon = epsilonFor<T1, T2>();
	for (size_t i = 0; i < data1.order(); i++) {
		if (!Value<double>::relative_distance_within(data1.getC(i), data2.getC(i), epsilon)) {
			return;
		}
		if (!Value<double>::relative_distance_within(data1.getD(i), data2.getD(i), epsilon)) {
			return;
		}
	}
	CPPUNIT_FAIL("Coefficients do not have equal values");
}

void TestIirCoefficients::testAssignmentSame()
{
	FixedIirCoefficients<float, 4> ff1;
	IirCoefficients<float> if1(4);
	FixedIirCoefficients<double, 4> fd1;
	IirCoefficients<double> id1(4);

	ff1.assign(ff1);
	if1.assign(if1);
	fd1.assign(fd1);
	id1.assign(id1);
}

void TestIirCoefficients::testAssignmentFloatFixedToNonFixed()
{
	FixedIirCoefficients<float, 4> ff1;
	IirCoefficients<float> if1(4);

	setRandomValues(ff1);
	setRandomValues(if1);
	assertNotEqual(ff1, if1);

	if1.assign(ff1);

	assertEqual(ff1, if1);
}

void TestIirCoefficients::testAssignmentFloatNonFixedToFixed()
{
	FixedIirCoefficients<float, 4> ff1;
	IirCoefficients<float> if1(4);

	setRandomValues(ff1);
	setRandomValues(if1);
	assertNotEqual(ff1, if1);

	ff1.assign(if1);

	assertEqual(ff1, if1);
}

void TestIirCoefficients::testAssignmentDoubleToFloat()
{
	FixedIirCoefficients<float, 4> ff1;
	IirCoefficients<float> if1(4);
	FixedIirCoefficients<double, 4> fd1;
	IirCoefficients<double> id1(4);

	setRandomValues(ff1);
	setRandomValues(if1);
	setRandomValues(fd1);
	setRandomValues(id1);

	assertNotEqual(ff1, fd1);
	assertNotEqual(if1, id1);

	ff1.assign(fd1);
	if1.assign(id1);

	assertEqual(ff1, fd1);
	assertEqual(if1, id1);
}

void TestIirCoefficients::testAssignmentFloatToDouble()
{
	FixedIirCoefficients<float, 4> ff1;
	IirCoefficients<float> if1(4);
	FixedIirCoefficients<double, 4> fd1;
	IirCoefficients<double> id1(4);

	setRandomValues(ff1);
	setRandomValues(if1);
	setRandomValues(fd1);
	setRandomValues(id1);

	assertNotEqual(ff1, fd1);
	assertNotEqual(if1, id1);

	fd1.assign(ff1);
	id1.assign(if1);

	assertEqual(ff1, fd1);
	assertEqual(if1, id1);
}

void TestIirCoefficients::testAssignmentFixedToNonFixedOther()
{
	FixedIirCoefficients<float, 4> ff1;
	IirCoefficients<float> if1(4);
	FixedIirCoefficients<double, 4> fd1;
	IirCoefficients<double> id1(4);

	setRandomValues(ff1);
	setRandomValues(if1);
	setRandomValues(fd1);
	setRandomValues(id1);

	assertNotEqual(fd1, if1);
	assertNotEqual(ff1, id1);

	fd1.assign(if1);
	ff1.assign(id1);

	assertEqual(fd1, if1);
	assertEqual(ff1, id1);
}

void TestIirCoefficients::testAssignmentNonFixedToFixedOther()
{
	FixedIirCoefficients<float, 4> ff1;
	IirCoefficients<float> if1(4);
	FixedIirCoefficients<double, 4> fd1;
	IirCoefficients<double> id1(4);

	setRandomValues(ff1);
	setRandomValues(if1);
	setRandomValues(fd1);
	setRandomValues(id1);

	assertNotEqual(fd1, if1);
	assertNotEqual(ff1, id1);

	fd1.assign(if1);
	ff1.assign(id1);

	assertEqual(fd1, if1);
	assertEqual(ff1, id1);
}

void TestIirCoefficients::testOrderForFixed()
{
	FixedIirCoefficients<float, 4> coeffs;

	CPPUNIT_ASSERT_EQUAL_MESSAGE("Initial order", (size_t)4, coeffs.order());
}

void TestIirCoefficients::testMaxOrderForFixed()
{
	FixedIirCoefficients<float, 4> coeffs;

	CPPUNIT_ASSERT_EQUAL_MESSAGE("Maximum order", (size_t)4, coeffs.maxOrder());
}

void TestIirCoefficients::testAssignmentSmallerOrderForFixed()
{
	FixedIirCoefficients<float, 4> four;
	FixedIirCoefficients<float, 5> five;

	CPPUNIT_ASSERT_THROW_MESSAGE("Fixed-size coefficients throws exception on smaller order assign", five.assign(four), std::invalid_argument);
}

void TestIirCoefficients::testAssignmentBiggerOrderForFixed()
{
	FixedIirCoefficients<float, 4> four;
	FixedIirCoefficients<float, 5> five;

	CPPUNIT_ASSERT_THROW_MESSAGE("Fixed-size coefficients throws exception on larger order assign", four.assign(five), std::invalid_argument);
}

void TestIirCoefficients::testSetOrder()
{
	IirCoefficients<float> coeffs(4);

	CPPUNIT_ASSERT_EQUAL_MESSAGE("Initial order", (size_t)4, coeffs.order());
	CPPUNIT_ASSERT_EQUAL_MESSAGE("Maximum order", (size_t)4, coeffs.maxOrder());

	for (size_t i = 1; i <= coeffs.maxOrder(); i++) {
		coeffs.setOrder(i);
		CPPUNIT_ASSERT_EQUAL_MESSAGE("Set order", i, coeffs.order());
	}
}

void TestIirCoefficients::testSetOrderZero()
{
	IirCoefficients<float> coeffs(4);

	CPPUNIT_ASSERT_THROW_MESSAGE("Exception thrown on setOrder(0)", coeffs.setOrder(0), std::invalid_argument);
}

void TestIirCoefficients::testSetOrderTooBig()
{
	IirCoefficients<float> coeffs(4);

	CPPUNIT_ASSERT_THROW_MESSAGE("Exception thrown on setOrder(order > max order)", coeffs.setOrder(coeffs.maxOrder() + 1), std::invalid_argument);
}

void TestIirCoefficients::testAssignBiggerOrder()
{
	IirCoefficients<float> small(4);
	IirCoefficients<float> big(4);

	setRandomValues(small);
	setRandomValues(big);

	small.setOrder(2);
	small.assign(big);

	CPPUNIT_ASSERT_EQUAL_MESSAGE("Bigger order adopted on assignment", big.order(), small.order());

	assertEqual(small, big);
}

void TestIirCoefficients::testAssignSmallerOrder()
{
	IirCoefficients<float> small(4);
	IirCoefficients<float> big(4);

	setRandomValues(small);
	setRandomValues(big);

	small.setOrder(2);
	big.assign(small);

	CPPUNIT_ASSERT_EQUAL_MESSAGE("Smaller order adopted on assignment", big.order(), small.order());

	assertEqual(small, big);
}

void TestIirCoefficients::testAssignTooBigOrder()
{
	IirCoefficients<float> small(4);
	IirCoefficients<float> tooBig(5);

	CPPUNIT_ASSERT_THROW_MESSAGE("Coefficients throws on assign with too big order", small.assign(tooBig), std::invalid_argument);
}
