/*
 * Main.cpp
 *
 * Exists to make linking of sanity checks succeed. It can also 
 * be used to actually perform sanity checks on the headers
 * provided in this project.
 *
 * Part of TdAP
 * Time-domain Audio Processing
 * Copyright (C) 2015 Michel Fleur.
 * Source https://bitbucket.org/emmef/tdap
 * Email  tdap@emmef.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <iostream>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TextTestRunner.h>

#include "TestIirCoefficients.hpp"
#include "TestArrayTraits.hpp"
#include "TestCount.hpp"
#include "TestDenormal.hpp"
#include "TestPower2.hpp"
#include "TestValue.hpp"
#include "TestFixedSizeArray.hpp"
#include "TestFixedCapArray.hpp"
#include "TestArray.hpp"
// __AUTO_ADD_TEST_INCLUDES_HERE__

using namespace CppUnit;
using namespace std;

int main ()
{
	CppUnit::TextTestRunner runner;

	runner.addTest(TestCount::createSuite());
	runner.addTest(TestPowerOf2::createSuite());
	runner.addTest(TestValue::createSuite());
	runner.addTest(TestIirCoefficients::createSuite());
	runner.addTest(test_denormal::createSuite());
	runner.addTest(TestArrayTraits::createSuite());
runner.addTest(TestFixedSizeArray::createSuite());
runner.addTest(TestFixedCapArray::createSuite());
runner.addTest(TestArray::createSuite());
// __AUTO_ADD_SUITE_HERE__

	cout << "Starting tests!" << endl;

	return runner.run() ? 0 : 1;
}
