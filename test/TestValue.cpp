/*
 * test_value.cpp
 *
 * Part of TdAP
 * Time-domain Audio Processing
 * Copyright (C) 2015 Michel Fleur.
 * Source https://bitbucket.org/emmef/tdap
 * Email  tdap@emmef.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../include/tdap/Value.hpp"
#include "TestValue.hpp"

using namespace tdap;

void TestValue::doesCompile()
{
	// Nothing to implement
}

void TestValue::setUp()
{
	// Nothing to implement
}

void TestValue::tearDown()
{
	// Nothing to implement
}

TestValue::~TestValue()
{
	// Nothing to implement
}

