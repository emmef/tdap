/*
 * TestArray.hpp
 *
 * Defines unit tests for tdap/Array.hpp.
 *
 * Part of TdAP
 * Time-domain Audio Processing
 * Copyright (C) 2015 Michel Fleur.
 * Source https://bitbucket.org/emmef/tdap
 * Email  tdap@emmef.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TEST__TDAP_ARRAY_HEADER_GUARD
#define TEST__TDAP_ARRAY_HEADER_GUARD

#include <cppunit/TestCaller.h>
#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>

/*
 * Do NOT include <tdap/Array.hpp> here as it will
 * slow down the build process considerably.
 * The file
 *   Array.cpp
 * that implements this test fixture will include the header.
 */
using namespace CppUnit;

class TestArray : public TestFixture
{
public:
	void testWithMaxSize();
	void testWithMaxSizeZero();
	void testWithMaxSizeTooLarge();
	void testWithSameSize();
	void testWithOtherSize();
	void testWithOtherSizeZero();
	void testWithOtherSizeTooLarge();

	void testSetSize();
	void testSetSizeZero();
	void testSetSizeTooLarge();

	void testInitialAssignment();
	void testInitialAssignmentDifferentSize();

	static TestSuite *createSuite()
	{
		CppUnit::TestSuite *suite = new CppUnit::TestSuite("Test suite for 'tdap/Array.hpp'");

		suite->addTest(
				new TestCaller<TestArray>("TestArray test 'testWithMaxSize'",
						&TestArray::testWithMaxSize));
		suite->addTest(
				new TestCaller<TestArray>("TestArray test 'testWithMaxSizeZero'",
						&TestArray::testWithMaxSizeZero));
		suite->addTest(
				new TestCaller<TestArray>("TestArray test 'testWithMaxSizeTooLarge'",
						&TestArray::testWithMaxSizeTooLarge));
		suite->addTest(
				new TestCaller<TestArray>("TestArray test 'testWithSameSize'",
						&TestArray::testWithSameSize));
		suite->addTest(
				new TestCaller<TestArray>("TestArray test 'testWithOtherSize'",
						&TestArray::testWithOtherSize));
		suite->addTest(
				new TestCaller<TestArray>("TestArray test 'testWithOtherSizeZero'",
						&TestArray::testWithOtherSizeZero));
		suite->addTest(
				new TestCaller<TestArray>("TestArray test 'testWithOtherSizeTooLarge'",
						&TestArray::testWithOtherSizeTooLarge));
		suite->addTest(
				new TestCaller<TestArray>("TestArray test 'testSetSize'",
						&TestArray::testSetSize));
		suite->addTest(
				new TestCaller<TestArray>("TestArray test 'testSetSizeZero'",
						&TestArray::testSetSizeZero));
		suite->addTest(
				new TestCaller<TestArray>("TestArray test 'testSetSizeTooLarge'",
						&TestArray::testSetSizeTooLarge));
		suite->addTest(
				new TestCaller<TestArray>("TestArray test 'testInitialAssignment'",
						&TestArray::testInitialAssignment));
		suite->addTest(
				new TestCaller<TestArray>("TestArray test 'testInitialAssignmentDifferentSize'",
						&TestArray::testInitialAssignmentDifferentSize));

		return suite;
	}

	void setUp();
	void tearDown();
	virtual ~TestArray();
};

#endif /* TEST__TDAP_ARRAY_HEADER_GUARD */
