/*
 * TestArrayTraits.hpp
 *
 * Defines unit tests for tdap/ArrayTraits.hpp.
 *
 * Part of TdAP
 * Time-domain Audio Processing
 * Copyright (C) 2015 Michel Fleur.
 * Source https://bitbucket.org/emmef/tdap
 * Email  tdap@emmef.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TEST__TDAP_ARRAYTRAITS_HEADER_GUARD
#define TEST__TDAP_ARRAYTRAITS_HEADER_GUARD

#include <cppunit/TestCaller.h>
#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>

/*
 * Do NOT include <tdap/ArrayTraits.hpp> here as it will
 * slow down the build process considerably.
 * The file
 *   ArrayTraits.cpp
 * that implements this test fixture will include the header.
 */
using namespace CppUnit;

class TestArrayTraits : public TestFixture
{
public:
	void doesCompile(); // tests if the header compiles by its sole existence

	static TestSuite *createSuite()
	{
		CppUnit::TestSuite *suite = new CppUnit::TestSuite("Test suite for 'tdap/ArrayTraits.hpp'");

		suite->addTest(
				new TestCaller<TestArrayTraits>("TestArrayTraits test 'doesCompile'",
						&TestArrayTraits::doesCompile));

		return suite;
	}

	void setUp();
	void tearDown();
	virtual ~TestArrayTraits();
};

#endif /* TEST__TDAP_ARRAYTRAITS_HEADER_GUARD */
